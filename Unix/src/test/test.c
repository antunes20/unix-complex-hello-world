/*
 * test.c
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */

#include "common.h"
#include "test/test.h"
#include <stdio.h>
#include <unistd.h>
/* test writing string to a file */
void testWriteLogFile(int fd){
	printf("Writing to log...\n");
	writeToLog(fd, "This is a test", getppid());
	printf("Writing done! Closing file...\n");
	//closeLog();
}
