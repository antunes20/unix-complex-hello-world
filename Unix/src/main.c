/*
 ============================================================================
 Name        : Unix.c
 Author      : José Magalhães
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "test/test.h"
#include "logic/logic.h"
#include "common.h"
#include "interface/interface.h"


int main(void) {
	srand (time(NULL));
	if(openLogfile() == -1)
		return EXIT_FAILURE;
	closeLogfile();
	mainMenu();
	closeLogfile();
	return EXIT_SUCCESS;
}

