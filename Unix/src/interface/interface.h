/*
 * interface.h
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */

#ifndef INTERFACE_INTERFACE_H_
#define INTERFACE_INTERFACE_H_

#define OK       0
#define NO_INPUT 1
#define TOO_LONG 2
#define EXIT 3
/* displays the message, and waits for the user input */
int waitforuser(char * message);

/* main menu */
void mainMenu();

/* asks for a string and initiates the logic module */
void askForInput();

void showLogfile();
#endif /* INTERFACE_INTERFACE_H_ */
