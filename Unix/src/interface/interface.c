/*
 * interface.c
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */

#include "interface/interface.h"
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"
#include "logic/logic.h"

static int mygetch();

/* clears the screen */
static void clear_screen();

static int getLine (char *prmpt, char *buff, size_t sz);

/* displays the message, and waits for the user to press
   return */
int waitforuser(char * message) {
	char buf[10];
	printf("%s", message);
	fflush(stdout);
	fgets(buf, 9, stdin);
	buf[strlen(buf) - 1] = '\0';
	if(strcmp(buf,"Y") == 0 || strcmp(buf, "Yes") == 0 || strcmp(buf, "yes") == 0 || strcmp(buf, "y") == 0)
		return 0;
	else return 1;
}

void mainMenu(){
	unsigned int op;
	char buff [12];
	int error;
	int read;
	do{
		clear_screen();
		op = 0;
		printf("*** MAIN MENU ***\n\n");
		printf("1 - Enter a String\n");
		printf("2 - Show Logfile\n");
		printf("3 - Exit\n");
		error = getLine ("\n Operation: ", buff, sizeof(buff)-1);
		if (error) {
			continue;
		}
		else{
			read = sscanf(buff,"%u", &op);
		}
		if(read <= 0 || op > EXIT){
			continue;
		}
		switch(op){
		case 1: askForInput();
		break;
		case 2: showLogfile();
		break;
		}
	} while(op != EXIT);
}

/* asks for a string and initiates the logic module */
void askForInput(){
	char buffer[BUFFER_SIZE];
	int error;
	do{
		clear_screen();
		error = getLine ("Enter a sentence: ", buffer, sizeof(buffer));
		if (error == TOO_LONG) {
			printf ("Input too long [%s]\n", buffer);
			continue;
		}
		else if(error == NO_INPUT){
			return;
		}

	} while(error);
	init(buffer);
	run();
	printf("\n\nPress any key to continue ....");
	mygetch();
}

void showLogfile(){
	int fd = getFiledesc();
	if(fd != -1)
		readFromLog(fd);
	else printf("*** LogFile is empty ***");
	printf("\n\nPress any key to continue ....");
	mygetch();
}

static int mygetch()
{
	int ch;
	struct termios oldt, newt;

	tcgetattr ( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr ( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr ( STDIN_FILENO, TCSANOW, &oldt );

	return ch;
}
/* clears the screen */
static void clear_screen()
{
	system ("clear");
}

static int getLine (char *prmpt, char *buff, size_t sz) {
	int ch, extra;

	// Get line with buffer overrun protection.
	if (prmpt != NULL) {
		printf ("%s", prmpt);
		fflush (stdout);
	}
	if (fgets (buff, sz, stdin) == NULL || buff[0] == '\n')
		return NO_INPUT;

	// If it was too long, there'll be no newline. In that case, we flush
	// to end of line so that excess doesn't affect the next call.
	if (buff[strlen(buff)-1] != '\n') {
		extra = 0;
		while (((ch = getchar()) != '\n') && (ch != EOF))
			extra = 1;
		return (extra == 1) ? TOO_LONG : OK;
	}

	// Otherwise remove newline and give string back to caller.
	buff[strlen(buff)-1] = '\0';
	return OK;
}
