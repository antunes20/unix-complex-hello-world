/*
 * logic.h
 *
 *  Created on: Oct 31, 2014
 *      Author: user
 */

#ifndef LOGIC_LOGIC_H_
#define LOGIC_LOGIC_H_

#define PROCESS_GROUP 6969

/* inits */
int init(char* str);

/* runs */
int run();

/* block signal signo */
int blockSignal(int signo);
/* unblock signal signo */
int unblockSignal(int signo);
/* waits for a signal */
int waitForSignal();

/* writes to the log the termination status of each child */
void pr_exit(int status, int pid);

/* free some memory and close file descriptor fd */
void finalize ();

/* returns filedesciptor associated to logfile */
int getFiledesc();

int openLogfile();

void closeLogfile();

#endif /* LOGIC_LOGIC_H_ */
