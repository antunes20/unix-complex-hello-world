/*
 * logic.c
 *
 *  Created on: Oct 31, 2014
 *      Author: user
 */


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "common.h"
#include "logic.h"
#include "interface/interface.h"

static int sigpipe;
static int* pids;
/* file descriptor */
static int fd = -1;
/* children alive */
static int children;
struct sigaction action;
static int pipefd[2];
/* all child processes has terminated */
static int terminated;
/* string to be printed */
static char * str;
/* waits for child processes termination status */
static void waitForChildren();
/* SIGINT Handler */
static void sig_int_handler(int signo);
/* SIGUSR2 Handler */
static void sig_usr_handler(int signo);
/* SIGCHLD Handler */
static void sig_chld_handler(int signo);
/* sends a signal for all child processes*/
static void sendSignal(int signo);

int run() {
	int i = 0;
	char fd_s[5];
	char id_s[5];
	char pid_s [10];
	char total_s[5];

	/* Blocking SIGUSR1 to guarantee that the signal didn't arrive before sigsuspend call */
	if( blockSignal(SIGUSR1) == -1)
		return -1;
	if(blockSignal(SIGCHLD) == -1)
		return -1;
	for (; i < children ; i++){
		pids[i] = fork();
		if (pids[i] == -1){
			perror("run");
			return -1;
		}
		else if(pids[i] == 0)
			break;
	}
	/* converting to string to use in exec call */
	sprintf(fd_s,"%d", fd);
	sprintf(id_s,"%d", i);
	sprintf(total_s,"%d", children);

	if(i == children){
		if(unblockSignal(SIGCHLD) == -1)
			return -1;
		writeToLog(fd, "Signaling last Child...", getpid());
		kill(pids[children-1], SIGUSR1);
		writeToLog(fd, "Waiting for child processes to complete...", getpid());
		waitForSignal();
		terminated = 1;
		sendSignal(SIGUSR1);
		waitForChildren();
		writeToLog(fd, "All done. Goodbye!", getpid());
		finalize();
	}
	else if(i == 0){
		execl("../UnixChild/Debug/UnixChild","../UnixChild/Debug/UnixChild",fd_s, str, id_s, total_s, NULL);
		perror("exec failed");
		return -1;
	}
	else{
		sprintf(pid_s, "%d", pids[i-1]);
		//<file descriptor> <message to print> <process number> <total of processes>  [<pid>]
		execl("../UnixChild/Debug/UnixChild","../UnixChild/Debug/UnixChild", fd_s, str, id_s, total_s, pid_s, NULL);
		perror("exec failed");
		return -1;
	}
	return 0;
}

int openLogfile(){
	if(fd == -1){
		fd = openLog("logfile.txt");
		if(fd == -1){
			perror("init");
			return -1;
		}
		return 0;
	}
	else return 0;
}

void closeLogfile(){
	close(fd);
	fd = -1;
}
int init(char* _str){
	if(openLogfile() == -1){
		perror("init");
		return -1;
	}
	if(pipe(pipefd) != 0){
		perror("init");
		return -1;
	}
	sigpipe = pipefd[1];
	sigemptyset(&action.sa_mask); //all signals are delivered
	action.sa_handler = sig_usr_handler;
	action.sa_flags = 0;
	if(sigaction(SIGUSR2, &action,NULL) == -1){
		writeToLog(fd,strerror(errno), getpid());
		perror("init");
		return -1;
	}
	action.sa_handler = sig_chld_handler;
	if(sigaction(SIGCHLD, &action,NULL) == -1){
		writeToLog(fd,strerror(errno), getpid());
		perror("init");
		return -1;
	}
	action.sa_handler = sig_int_handler;
	if(sigaction(SIGINT, &action,NULL) == -1){
		writeToLog(fd,strerror(errno), getpid());
		perror("init");
		return -1;
	}
	terminated = 0;
	str = _str;
	children = (rand() % strlen(str)) + 1;
	pids = (int * ) malloc(sizeof(int)* children);
	return 0;
}

/* free some memory */
void finalize (){
	free(pids);
}

/* sends a signal for all child processes*/
static void sendSignal(int signo){
	int i;
	for(i = 0; i < children; i++){
		kill(pids[i], signo);
	}
}

static void killChildren(){
	int i;
	terminated = 1;
	if(blockSignal(SIGCHLD) == -1)
		perror("waitForChildren");
	for(i = 0; i < children; i++){
		kill(pids[i], SIGKILL);
	}
	if(unblockSignal(SIGCHLD) == -1)
		perror("waitForChildren");
	waitForChildren();
}

int blockSignal(int signo){
	sigset_t sigmask;
	if (sigprocmask(SIG_SETMASK,NULL,&sigmask)==-1) /* get mask */
	{
		perror("sigprocmask");
		return -1;
	}
	else
	{
		sigaddset(&sigmask,signo);
		if (sigprocmask(SIG_BLOCK, &sigmask, NULL)){
			perror("sigprocmask");
			return -1;
		}
	}
	return 0;
}

/* unblock signal signo */
int unblockSignal(int signo){
	sigset_t sigmask;
	if (sigprocmask(SIG_SETMASK,NULL,&sigmask)==-1) /* get mask */
	{
		perror("sigprocmask");
		return -1;
	}
	else
	{
		sigaddset(&sigmask,signo);
		if (sigprocmask(SIG_UNBLOCK, &sigmask, NULL)){
			perror("sigprocmask");
			return -1;
		}
	}
	return 0;
}

/* waits for a signal */
int waitForSignal(){
	for ( ; ; ) {
		char mysignal;
		int res = read(pipefd[0],&mysignal,1);
		// When read is interrupted by a signal, it will return -1 and errno is EINTR.
		if (res == 1) {
			return mysignal;
		}
	}
	return -1;
}

/* writes to the log the termination status of each child */
void pr_exit(int status, int pid)
{
	char buffer [BUFFER_SIZE];
	if (WIFEXITED(status)){
		sprintf(buffer, "normal termination of process %d, exit status = %d",pid, WEXITSTATUS(status));
		writeToLog(fd,buffer, getpid());
	}
	else if(WIFSIGNALED(status)){
		sprintf(buffer, "abnormal termination of process %d, signal number = %d%s",pid, WTERMSIG(status),
#ifdef WCOREDUMP
				WCOREDUMP(status) ? " (core file generated)" : "");
#else
		"");
#endif
		writeToLog(fd,buffer, getpid());
	}
	else if (WIFSTOPPED(status)){
		sprintf(buffer, "child %d stopped, signal number = %d", pid, WSTOPSIG(status));
	}
}

int getFiledesc(){
	return fd;
}

/* waits for child processes termination status */
static void waitForChildren(){
	int i;
	int status;
	if(blockSignal(SIGCHLD) == -1)
		perror("waitForChildren");
	for(i = 0; i < children; i++){
		waitpid(pids[i],&status, 0);
		pr_exit(status, pids[i]);
	}
	if(unblockSignal(SIGCHLD) == -1)
		perror("waitForChildren");
}

/* SIGINT Handler */
static void sig_int_handler(int signo) {
	int rsp = waitforuser("Are you sure you want to quit? (Y/n)?: ");
	if(!rsp){
		killChildren();
		finalize();
		exit (EXIT_SUCCESS);
	}
}

/* SIGUSR2 Handler */
static void sig_usr_handler(int signo) {
	char ret = signo;
	write(sigpipe,&ret,1);
}

/* SIGCHLD Handler */
static void sig_chld_handler(int signo) {
	if(!terminated){
		writeToLog(fd, "Something went wrong with a child. terminating...", getpid());
		killChildren();
		finalize();
		exit (EXIT_FAILURE);
	}
}
