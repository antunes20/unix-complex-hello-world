/*
 * api.c
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "common.h"

/* open the file */
int openLog(char* name){
	int fd;
	/* set up a file to lock */
	fd = open(name, O_RDWR | O_CREAT | O_TRUNC, 0666);
	if (fd < 0) {
		perror("openLog");
		return -1;
	}
	return fd;
}

/* closes log file */
void closeLog(int fd){
	close(fd);
	fd = -1;
}

/* writes buffer to logfile */
void writeToLog(int fd, char* buffer, int pid){
	time_t rawtime;
	struct tm * timeinfo;
	char time_s[BUFFER_SIZE];
	char owner_s[BUFFER_SIZE];
	/* print to owner pid */
	sprintf (owner_s,"[PID: %u]", pid);
	/*get the lock for writing */
	getlock(fd, F_WRLCK);
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	/* print time and date */
	sprintf (time_s,"[%s", asctime (timeinfo) );
	/* removing newline */
	time_s[strlen(time_s) -1] = '\0';
	strcat(owner_s, time_s);
	strcat(owner_s,"]: ");
	if (write(fd, owner_s, strlen (owner_s)) <= 0) {
		perror("writeToLog");
		return;
	}
	if (write(fd, buffer, strlen (buffer)) <= 0) {
		perror("writeToLog");
		return;
	}
	if (write(fd, "\n", 1) <= 0) {
		perror("writeToLog");
		return;
	}
	getlock(fd, F_UNLCK);
}

/* reads Logfile and prints to the screen */
void readFromLog(int fd){
	unsigned char buffer[BUFFER_SIZE];
	int nr, nw;
	getlock(fd, F_RDLCK);
	while ((nr = read(fd, buffer, BUFFER_SIZE)) > 0) {
		if ((nw = write(STDOUT_FILENO, buffer, nr)) <= 0 || nw != nr) {
			perror("readFromLog");
		}
	}
	getlock(fd, F_UNLCK);
}

/* Gets a lock of the indicated type on the fd which is passed.
   The type should be either F_UNLCK, F_RDLCK, or F_WRLCK */
void getlock(int fd, int type) {
	struct flock lockinfo;

	/* we'll lock the entire file */
	lockinfo.l_whence = SEEK_SET;
	lockinfo.l_start = 0;
	lockinfo.l_len = 0;

	/* keep trying until we succeed */
	while (1) {
		lockinfo.l_type = type;
		/* if we get the lock, return immediately */
		if (!fcntl(fd, F_SETLKW, &lockinfo)) return;
		perror("getlock");

	}
}
