/*
 * api.h
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */

#ifndef API_H_
#define API_H_

#define BUFFER_SIZE 512
/* open the file */
int openLog(char* name);

/* closes log file */
void closeLog(int fd);

/* writes buffer to logfile */
void writeToLog(int fd, char* buffer, int pid);

/* reads Logfile and prints to the screen */
void readFromLog(int fd);

/* Gets a lock of the indicated type on the fd which is passed.
   The type should be either F_UNLCK, F_RDLCK, or F_WRLCK */
void getlock(int fd, int type);

#endif /* API_H_ */
