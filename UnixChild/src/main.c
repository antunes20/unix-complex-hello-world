/*
 ============================================================================
 Name        : UnixChild.c
 Author      : José Magalhães
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "common.h"

/* useful to calculate the limits */
#define BLOCK_LOW(id,p,n)   ((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n)  (BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n)  (BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+1)

/* file descriptor */
static int fd;

/* siguser handler*/
void siguser_handler(int signo);

int main(int argc, char* argv[]) {
	struct sigaction action;
	sigset_t sigmask;
	/*index to start to print */
	size_t min_index;
	/*index to stop to print */
	size_t max_index;
	/*process number*/
	int id;
	/*total of processes */
	int ptotal;
	/*process previously created */
	int brother_pid = 0;
	int k;

	/*message to print */
	size_t size;
	if (argc != 5 && argc != 6) {
		printf("Usage: %s <file descriptor> <message to print> <process number> <total of processes>  [<pid>]\n", argv[0]);
		return EXIT_FAILURE;
	}

	fd = atoi(argv[1]);
	id = atoi(argv[3]);
	ptotal = atoi(argv[4]);
	/* validating input */
	if(id < 0 || id >= ptotal){
		printf("Process number should be > -1 and less than total of processes\n");
		return EXIT_FAILURE;
	}
	if(ptotal < 1 || ptotal <= id){
		printf("Total of processes should be > 0 and greater than process number\n");
		return EXIT_FAILURE;
	}
	writeToLog(fd,"Initiated!", getpid());
	size = strlen(argv[2]);
	if(argc == 6)
		brother_pid = atoi(argv[5]); // the brother pid to signal
	/* inverting id to guarantee that <message to print> is printed correctly (from the left to the right) */
	id = ptotal - (id + 1);
	sigemptyset(&action.sa_mask); //all signals are delivered
	action.sa_handler = siguser_handler;
	action.sa_flags = 0;
	if(sigaction(SIGUSR1, &action,NULL) == -1){
		writeToLog(fd,strerror(errno), getpid());
		perror("Child");
		return EXIT_FAILURE;
	}
	sigfillset(&sigmask); //all signals blocked
	writeToLog(fd,"Waiting for signal", getpid());
	sigdelset(&sigmask,SIGUSR1); // except SIGUSR1
	sigsuspend(&sigmask); //suspend until siguser_handler executes
	min_index = BLOCK_LOW(id, ptotal, size);
	max_index = BLOCK_HIGH(id, ptotal, size);
	for(k = min_index; k <= max_index; k++){
		// using write to print immediately
		write(STDOUT_FILENO, &(argv[2][k]), 1);
	}
	writeToLog(fd,"finished to write.", getpid());
	if(brother_pid){
		writeToLog(fd,"Signaling brother...", getpid());
		kill(brother_pid, SIGUSR1);
	}
	else{
		/* notify parent process. all work is done */
		writeToLog(fd,"Signaling parent process. All work is done ", getpid());
		kill(getppid(), SIGUSR2);
	}
	writeToLog(fd,"Waiting for parent signal", getpid());
	sigsuspend(&sigmask); //suspend until other processes finished their tasks
	return EXIT_SUCCESS;
}

void siguser_handler(int signo){

}
