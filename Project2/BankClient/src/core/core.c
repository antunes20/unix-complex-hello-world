/*
 * core.c
 *
 *  Created on: Nov 30, 2014
 *      Author: user
 */


#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include <limits.h>
#include <mqueue.h>


#include "core.h"
#include "util/common.h"
#include "util/monitor.h"

static void *shared_memory;
static sh_memdata *shmem;
static int desks;
static char queue_name[50];
static mqd_t client_queue;

void initClient(){
	int shmid;
	int mem_size;
	int client;
	char number_s[20];
	struct mq_attr mqstat;

	/* Find shared memory segment */
	assert((shmid = shm_open(SH_MEMORY, O_RDWR, 0)) != -1);
	assert((shared_memory = mmap(NULL,sizeof(sh_memdata),PROT_READ|PROT_WRITE,
			MAP_SHARED,shmid, 0)) != MAP_FAILED);

	/* Find semaphores and buffer from memory */
	shmem = (sh_memdata *)shared_memory;
	startWrite(&(shmem->lock));
	desks = shmem->desks;
	client = shmem->clients;
	shmem->clients++;
	endWrite(&(shmem->lock));
	munmap(shared_memory, sizeof(sh_memdata));
	mem_size = sizeof(Monitor_s) + sizeof(size_t)*(desks + 2);
	assert((shared_memory = mmap(NULL,sizeof(mem_size),PROT_READ|PROT_WRITE,
			MAP_SHARED,shmid, 0)) != MAP_FAILED);
	shmem = (sh_memdata *)shared_memory;
	strcpy(queue_name, CLIENT_QUEUE_NAME);
	sprintf(number_s, "%d", client);
	strcat(queue_name, number_s);
	memset(&mqstat, 0, sizeof(mqstat));
	mqstat.mq_maxmsg = MAX_MSG;
	mqstat.mq_msgsize = MSG_SIZE;
	mqstat.mq_flags = 0;
	client_queue = mq_open(queue_name, O_CREAT|O_RDWR, MQ_MODE, &mqstat);
}

void requestOperation(int op, int id, int id2, float amount){
	msg_t* msg;
	size_t mlen;
	size_t prio;
	char desk_queue[50];
	char number_s[20];
	int i;
	size_t desk;
	mqd_t queue;

	desk = UINT_MAX;
	mlen = strlen(queue_name);
	msg = (msg_t*) malloc(sizeof(int)*3 + sizeof(float) + strlen(queue_name));
	if(op == TRANSFER_OP)
		msg->id2 = id2;
	msg->id = id;
	msg->mtype = op;
	if(op != BALANCE_OP)
		msg->amount = amount;
	prio = PRIORITY_LOW;
	memcpy(msg->response, queue_name, mlen);
	mlen += sizeof(int)*3 + sizeof(float);

	startRead(&(shmem->lock));
	for(i = 0; i < desks; i++){
		if(shmem->queue[i] < desk)
			desk = shmem->queue[i];
	}
	endRead(&(shmem->lock));
	startWrite(&(shmem->lock));
	shmem->queue[desk]++;
	endWrite(&(shmem->lock));
	strcpy(desk_queue, DESK_QUEUE_NAME);
	sprintf(number_s, "%d", desk);
	strcat(desk_queue, number_s);
	queue = mq_open(desk_queue,O_WRONLY);
	if(mq_send(queue, (char *) msg, mlen, prio) != 0) /* send message to process */
		perror("sendMessageToDesk");
	free(msg);
	mq_close(queue);
}

void closeClient(){
	mq_unlink(queue_name);
}
