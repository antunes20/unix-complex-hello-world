/*
 * common.h
 *
 *  Created on: Nov 29, 2014
 *      Author: user
 */

#ifndef UTIL_COMMON_H_
#define UTIL_COMMON_H_

#include "monitor.h"

#define MAX_MSG 10
#define MSG_SIZE sizeof(int)*3 + sizeof(float) + 50
#define MQ_MODE 0777
#define BALANCE_OP 200
#define WITHDRAW_OP 201
#define DEPOSIT_OP 202
#define TRANSFER_OP 203
#define UNKNOWN_OP 999
#define TERMINATE_OP 205
#define DESK_QUEUE_NAME "/deskqueue"
#define CLIENT_QUEUE_NAME "/clientqueue"
#define SH_MEMORY "bankshared"
#define PRIORITY_LOW 1
#define PRIORITY_MEDIUM 2
#define PRIORITY_HIGH 3

#define OK -1
#define ERROR -2
#define NO_MONEY -100
#define NO_ACCOUNT -101

typedef struct {
	int mtype;
	int id;
	int id2;
	float amount;
	char response[1];
} msg_t;

typedef struct {
  Monitor_s lock;
  size_t desks;
  size_t clients;
  size_t queue[1];
} sh_memdata;

#endif /* UTIL_COMMON_H_ */
