/*
 * test.c
 *
 *  Created on: Nov 30, 2014
 *      Author: user
 */

#include "test.h"
#include <pthread.h>
#include "core/core.h"
#include "util/common.h"

static void* threadBalanceRequests(void* arg);

void spamRequests(int nrThreads){
	pthread_t threads[nrThreads];
	int i;
	pthread_t thread_id;
	void* thread_result;
	initClient();
	for(i = 0; i < nrThreads; i++){
		requestOperation(BALANCE_OP,1,0,0);
	}
	/*for(i= 0; i< nrThreads; i++){
		pthread_join(threads[i], thread_result);
	}*/
	closeClient();
}

static void* threadBalanceRequests(void* arg){
	requestOperation(BALANCE_OP,1,0,0);
	return 0;
}
