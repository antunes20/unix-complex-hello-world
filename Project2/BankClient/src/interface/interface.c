/*
 * interface.c
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */


#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

#include "interface.h"
#include "util/common.h"
#include "core/core.h"

static int mygetch();

/* clears the screen */
static void clear_screen();

static int getLine (char *prmpt, char *buff, size_t sz);

/* displays the message, and waits for the user to press
 * return */
int waitforuser(char * message) {
	char buf[10];
	printf("%s", message);
	fflush(stdout);
	fgets(buf, 9, stdin);
	buf[strlen(buf) - 1] = '\0';
	if(strcmp(buf,"Y") == 0 || strcmp(buf, "Yes") == 0 || strcmp(buf, "yes") == 0 || strcmp(buf, "y") == 0)
		return 0;
	else return 1;
}

void mainMenu(){
	unsigned int op;
	char buff [12];
	int error;
	int read;
	initClient();
	do{
		clear_screen();
		op = 0;
		printf("*** MAIN MENU ***\n\n");
		printf("1 - Show Balance\n");
		printf("2 - Deposit\n");
		printf("3 - Withdraw\n");
		printf("4 - Transfer\n");
		printf("5 - Exit\n");
		error = getLine ("\n Operation: ", buff, sizeof(buff)-1);
		if (error) {
			continue;
		}
		else{
			read = sscanf(buff,"%u", &op);
		}
		if(read <= 0 || op > EXIT){
			continue;
		}
		switch(op){
		case 1: balanceMenu();
		break;
		case 2: depositMenu();
		break;
		case 3: withdrawMenu();
		break;
		case 4: transferMenu();
		break;
		}
	} while(op != EXIT);
	closeClient();
}

/* */
void balanceMenu(){
	char buffer[BUFFER_SIZE];
	int error;
	int value;
	int read;
	clear_screen();
	do{
		error =  getLine ("Account id: ", buffer, sizeof(buffer));
		if (error == TOO_LONG) {
			printf ("Input too long [%s]\n", buffer);
			continue;
		}
		else if(error == NO_INPUT){
			return;
		}
		read = sscanf(buffer,"%d", &value);
		if(read <= 0){
			printf("Please enter a number > 0!\n");
			error = 1;
		}
	} while(error);
	requestOperation(BALANCE_OP, value, -1, -1);
	printf("\n\nPress any key to continue ....");
	mygetch();
}

void depositMenu(){

}

void transferMenu(){

}

void withdrawMenu(){

}
/*void showLogfile(){
	int fd = getFiledesc();
	clear_screen();
	if(fd != -1){
		fd = openLogForReading(LOGFILE_NAME);
		readFromLog(fd);
	}
	else printf("*** Logfile is empty ***");
	printf("\n\nPress any key to continue ....");
	mygetch();
}*/

static int mygetch()
{
	int ch;
	struct termios oldt, newt;

	tcgetattr ( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr ( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr ( STDIN_FILENO, TCSANOW, &oldt );

	return ch;
}
/* clears the screen */
static void clear_screen()
{
	system ("clear");
}

static int getLine (char *prmpt, char *buff, size_t sz) {
	int ch, extra;

	/* Get line with buffer overrun protection. */
	if (prmpt != NULL) {
		printf ("%s", prmpt);
		fflush (stdout);
	}
	if (fgets (buff, sz, stdin) == NULL || buff[0] == '\n')
		return NO_INPUT;

	/* If it was too long, there'll be no newline. In that case, we flush
	 * to end of line so that excess doesn't affect the next call. */
	if (buff[strlen(buff)-1] != '\n') {
		extra = 0;
		while (((ch = getchar()) != '\n') && (ch != EOF))
			extra = 1;
		return (extra == 1) ? TOO_LONG : I_OK;
	}

	/* Otherwise remove newline and give string back to caller. */
	buff[strlen(buff)-1] = '\0';
	return I_OK;
}
