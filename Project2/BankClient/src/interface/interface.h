/*
 * interface.h
 *
 *  Created on: Oct 30, 2014
 *      Author: user
 */

#ifndef INTERFACE_INTERFACE_H_
#define INTERFACE_INTERFACE_H_

#define I_OK       0
#define NO_INPUT 1
#define TOO_LONG 2
#define EXIT 5
#define BUFFER_SIZE 500
/* displays the message, and waits for the user input */
int waitforuser(char * message);

/* main menu */
void mainMenu();

/* asks for an account balance */
void balanceMenu();

void depositMenu();

void transferMenu();

void withdrawMenu();

#endif /* INTERFACE_INTERFACE_H_ */
