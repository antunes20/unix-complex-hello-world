#include "desk.h"
#include "core/core.h"
#include "util/common.h"
#include "bank.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <assert.h>
#include <sys/mman.h>

static void process_request(msg_t* msg, char response[]);

void* desk_routine(void *arg){

	size_t msize;     /* amount of reserved space */
	msg_t *msg;
	int mlen;         /* actual length */
	mqd_t queue;
	mqd_t client_queue;
	unsigned prio;
	struct mq_attr mqstat;
	char response[500];
	int id;
	sh_memdata *shmem;
	memset(&mqstat, 0, sizeof(mqstat));
	mqstat.mq_maxmsg = MAX_MSG;
	mqstat.mq_msgsize = MSG_SIZE;
	mqstat.mq_flags = 0;
	id = atoi(arg + strlen(DESK_QUEUE_NAME));

	shmem = get_shared_memory();

	queue = mq_open((char *)arg, O_CREAT|O_RDWR, MQ_MODE, &mqstat);

	if(queue == -1) {
		perror("Failed to open message queue");
		return 0;
	}

	/* allocate space for message */
	msize = 50;

	assert((msg = (msg_t *)malloc(MSG_SIZE + 1)));
	msize = MSG_SIZE + 1;
	do {
		/* Get messages for this process ! */
		if ((mlen = mq_receive(queue, (char *) msg, msize, &prio)) == -1) {
			if (errno == EINTR) continue;

			if (errno == EMSGSIZE) {
				free(msg);
				msize *= 2;
				printf("%d message length -> %d\n", getpid(),(int) msize);
				assert((msg = (msg_t *)malloc(sizeof(int)*3 + sizeof(float) + msize + 1)));
				continue;
			}
			assert(errno == 0);
		}
		startWrite(&(shmem->lock));
		shmem->queue[id]--;
		endWrite(&(shmem->lock));
		msg->response[mlen - (sizeof(int)*3 + sizeof(float))] = (char) 0;
		process_request(msg, response);
		if(strcmp(response, "") == 0)
			break;
		printf("RESPONSE: %s\n", response);
		//client_queue = mq_open(msg->response,O_WRONLY);
		//if(mq_send(client_queue, (char *) response, strlen(response), prio) != 0) /* send message to client */
			//perror("desk_routine");
		//mq_close(client_queue);
	} while (mlen != 0);
	mq_close(queue);
	mq_unlink((char *)arg);
	free(msg);
	free(arg);
	return 0;
}

static void process_request(msg_t* msg, char response[]){
	int result;
	float balance;
	switch(msg->mtype){
	case BALANCE_OP:
		result = getAccountBalance(msg->id, &balance);
		if(result == OK)
			sprintf(response,"Balance Op: Account <%d> is %f", msg->id, balance);
		else if(result == NO_ACCOUNT)
			sprintf(response,"Balance Op: Account <%d> doesn't exit.", msg->id);
		break;
	case WITHDRAW_OP:
		result = withdraw(msg->id, msg->amount);
		if(result == OK)
			sprintf(response,"Withdraw Op: Withdraw of %f on account <%d> processed", msg->amount, msg->id);
		else if(result == NO_ACCOUNT)
			sprintf(response,"Withdraw Op: Account <%d> doesn't exit", msg->id);
		else if(result == NO_MONEY)
			sprintf(response,"Withdraw Op: Account <%d> doesn't have enough money", msg->id);
		break;
	case DEPOSIT_OP:
		result = deposit(msg->id, msg->amount);
		if(result == OK)
			sprintf(response,"Deposit Op: Deposit of %f on account <%d> processed", msg->amount, msg->id);
		else if(result == NO_ACCOUNT)
			sprintf(response,"Deposit Op: Account <%d> doesn't exit", msg->id);
		break;
	case TRANSFER_OP:
		result = transfer(msg->id, msg->id2, msg->amount);
		if(result == OK)
			sprintf(response,"Transfer Op: Transfer of %f from account <%d> to account <%d> processed", msg->amount, msg->id, msg->id2);
		else if(result == NO_ACCOUNT)
			sprintf(response,"Transfer Op: Account <%d> or Account <%d> doesn't exit", msg->id, msg->id2);
		else if(result == NO_MONEY)
			sprintf(response,"Transfer Op: Account <%d> doesn't have enough money", msg->id);
		break;
	case TERMINATE_OP:
		response[0] = '\0';
		break;
	default:
		result = UNKNOWN_OP;
		sprintf(response,"Unknown operation");
		break;
	}
}
