/*
 * bank.c
 *
 *  Created on: Nov 30, 2014
 *      Author: user
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include <mqueue.h>

#include "desk/bank.h"
#include "desk/desk.h"
#include "test/test.h"
#include "core/core.h"

static void *shared_memory;
static sh_memdata *shmem;

static int desks;
struct sigaction action;
sigset_t sigmask;
static pthread_t* threads;
/* SIGINT Handler */
static void sig_int_handler(int signo);
static int install_Handlers();
static int blockSignal(int signo);
static int unblockSignal(int signo);

void init_bank(int d){
	int i;
	int shmid;
	pthread_t thread_id;
	size_t mem_size;

	install_Handlers();
	if( blockSignal(SIGINT) == -1)
		exit(EXIT_FAILURE);
	initAccounts();
	createAccount(10);
	createAccount(20);
	desks = d;
	threads = (pthread_t*) malloc(sizeof(pthread_t) * desks);

	/* Allocate a shared memory segment. */
	mem_size = sizeof(Monitor_s) + sizeof(size_t)*(desks + 2);
	assert((shmid = shm_open(SH_MEMORY, O_CREAT|O_RDWR, SHM_R | SHM_W)) != -1);
	assert((ftruncate(shmid,mem_size)) != -1);
	assert((shared_memory = mmap(NULL, mem_size, PROT_READ|PROT_WRITE,
			MAP_SHARED,shmid, 0)) != MAP_FAILED);

	/* Initialize semaphores to shared memory section. */
	shmem = (sh_memdata *)shared_memory;
	initMonitor(&(shmem->lock));
	shmem->desks = desks;
	shmem->clients = 0;
	for(i = 0; i < desks; i++){
		char* queue_name = (char*) malloc(sizeof(char) * 50);
		char* number_s = (char*) malloc(sizeof(char) * 10);
		shmem->queue[i] = 0;
		strcpy(queue_name, DESK_QUEUE_NAME);
		sprintf(number_s, "%d", i);
		strcat(queue_name, number_s);
		pthread_create(&thread_id, NULL, desk_routine, queue_name);
		threads[i] = thread_id;
		//printf("Thread %lu queue_name %s\n", thread_id, queue_name);
	}
	if(unblockSignal(SIGINT) == -1)
		exit(EXIT_FAILURE);
	printf("Bank is ready.\n");
	sigfillset(&sigmask); /* all signals blocked */
	sigdelset(&sigmask, SIGINT); /* except SIGINT */
	sigsuspend(&sigmask); /* suspend until siguser_handler executes */
}

int get_nr_desks(){
	return desks;
}

sh_memdata* get_shared_memory(){
	return shmem;
}

static void sig_int_handler(int signo){
	int i;
	unsigned int prio;
	mqd_t queue;
	void *thread_result;
	msg_t* msg = (msg_t*) malloc(sizeof(msg_t) + 1);
	char* queue_name = (char*) malloc(sizeof(char) * 50);
	char* number_s = (char*) malloc(sizeof(char) * 10);
	msg->mtype = TERMINATE_OP;
	prio = PRIORITY_HIGH;
	for(i= 0; i < desks; i++){
		strcpy(queue_name, DESK_QUEUE_NAME);
		sprintf(number_s, "%d", i);
		strcat(queue_name, number_s);
		queue = mq_open(queue_name,O_WRONLY);
		startWrite(&(shmem->lock));
		shmem->queue[i]++;
		endWrite(&(shmem->lock));
		if(mq_send(queue, (char*) msg, sizeof(msg_t) + 1, prio) != 0){
			perror("sig_int_handler");
			exit(EXIT_FAILURE);
		}
		mq_close(queue);
	}

	for(i= 0; i< desks; i++){
		pthread_join(threads[i], thread_result);
	}

	free(queue_name);
	free(number_s);
	free(msg);
}

static int install_Handlers(){

	sigemptyset(&action.sa_mask); /* all signals are delivered */
	action.sa_flags = 0;
	action.sa_handler = sig_int_handler;
	if(sigaction(SIGINT, &action,NULL) == -1){
		perror("install_Handlers");
		return -1;
	}
	/* generate the seed*/
	srand (time(NULL));
	return 0;
}

static int blockSignal(int signo){
	sigset_t sigmask;
	if (sigprocmask(SIG_SETMASK,NULL,&sigmask)==-1) /* get mask */
	{
		perror("sigprocmask");
		return -1;
	}
	else
	{
		sigaddset(&sigmask,signo);
		if (sigprocmask(SIG_BLOCK, &sigmask, NULL)){
			perror("sigprocmask");
			return -1;
		}
	}
	return 0;
}

/* unblock signal signo */
static int unblockSignal(int signo){
	sigset_t sigmask;
	if (sigprocmask(SIG_SETMASK,NULL,&sigmask)==-1) /* get mask */
	{
		perror("sigprocmask");
		return -1;
	}
	else
	{
		sigaddset(&sigmask,signo);
		if (sigprocmask(SIG_UNBLOCK, &sigmask, NULL)){
			perror("sigprocmask");
			return -1;
		}
	}
	return 0;
}
