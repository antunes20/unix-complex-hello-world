/*
 * bank.h
 *
 *  Created on: Nov 30, 2014
 *      Author: user
 */

#ifndef DESK_BANK_H_
#define DESK_BANK_H_

#include "util/common.h"

int get_nr_desks();
void init_bank(int desks);
sh_memdata* get_shared_memory();
#endif /* DESK_BANK_H_ */
