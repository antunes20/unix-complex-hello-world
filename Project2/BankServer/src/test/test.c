/*
 * test.c
 *
 *  Created on: Nov 29, 2014
 *      Author: user
 */

#include "test.h"
#include <pthread.h>
#include "desk/desk.h"
#include "util/common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <mqueue.h>
#include <assert.h>
#include <unistd.h>

void sendMessageToDesk(char* txt){
	pthread_t thread_id;
	void *thread_result;
	mqd_t queue;
	unsigned prio;
	size_t mlen;
	msg_t *mess;
	mq_unlink(MQ_NAME);
	assert((pthread_create(&thread_id, NULL, desk_routine, MQ_NAME)) == 0);
	sleep(1);
	queue = mq_open(MQ_NAME,O_WRONLY);
	if(queue == -1)
		perror("sendMessageToDesk");
	mlen = strlen(txt);
	assert((mess = (msg_t *) malloc(sizeof(int)*3 + sizeof(float) + mlen)));
	mess->mtype = BALANCE_OP;
	mess->id = 1;
	mess->id2 = 2;
	mess->amount = 20;
	memcpy(mess->response, txt, mlen);
	mlen += sizeof(int)*3 + sizeof(float);
	prio = 2;
	if(mq_send(queue, (char *) mess, mlen, prio) != 0) /* send message to process */
		perror("sendMessageToDesk");
	free(mess);
	 assert((pthread_join(thread_id,&thread_result)) == 0);
}
