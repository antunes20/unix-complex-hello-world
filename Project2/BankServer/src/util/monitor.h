/*
 * monitor.h
 *
 *  Created on: Nov 28, 2014
 *      Author: user
 */

#ifndef UTIL_MONITOR_H_
#define UTIL_MONITOR_H_

#include <pthread.h>

typedef struct Monitor {
    pthread_cond_t OktoRead;
    pthread_cond_t OktoWrite;
    pthread_mutex_t lock;
    unsigned int writers;
    unsigned int readers;
    unsigned int waitingWriters;
    unsigned int waitingReaders;
    int stopped;
} Monitor_s;

void stopMonitor(Monitor_s* monitor);
void initMonitor(Monitor_s* monitor);
void startRead(Monitor_s* lock);
void startWrite(Monitor_s* lock);
void endRead(Monitor_s* lock);
void endWrite(Monitor_s* lock);

#endif /* UTIL_MONITOR_H_ */
