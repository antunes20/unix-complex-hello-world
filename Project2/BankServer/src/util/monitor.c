/*
 * monitor.c
 *
 *  Created on: Nov 28, 2014
 *      Author: user
 */

#include "monitor.h"

void initMonitor(Monitor_s* monitor){
	monitor->readers = 0;
	monitor->writers = 0;
	monitor->waitingReaders = 0;
	monitor->waitingWriters = 0;
	monitor->stopped = 0;
	pthread_mutex_init(&(monitor->lock), NULL);
	pthread_cond_init(&(monitor->OktoRead), NULL);
	pthread_cond_init(&(monitor->OktoWrite), NULL);
}

void stopMonitor(Monitor_s* monitor) {
	pthread_mutex_lock(&(monitor->lock));
	monitor->stopped = 1;
	pthread_cond_broadcast(&(monitor->OktoRead));
	pthread_cond_broadcast(&(monitor->OktoWrite));
	pthread_mutex_unlock(&(monitor->lock));
}

void startRead(Monitor_s* monitor){
	pthread_mutex_lock(&(monitor->lock));
	if (monitor->stopped)
		return;
	monitor->waitingReaders++;
	while(monitor->writers || monitor->waitingWriters){
		pthread_cond_wait(&(monitor->OktoRead), &(monitor->lock));
		if (monitor->stopped)
			return;
	}
	monitor->waitingReaders--;
	monitor->readers++;
	pthread_cond_signal(&(monitor->OktoRead));
	pthread_mutex_unlock(&(monitor->lock));
}

void startWrite(Monitor_s* monitor){
	pthread_mutex_lock(&(monitor->lock));
	if (monitor->stopped)
		return;
	monitor->waitingWriters++;
	while(monitor->writers || monitor->readers){
		pthread_cond_wait(&(monitor->OktoWrite), &(monitor->lock));
		if (monitor->stopped)
			return;
	}
	monitor->waitingWriters--;
	monitor->writers++;
	pthread_mutex_unlock(&(monitor->lock));
}

void endRead(Monitor_s* monitor){
	pthread_mutex_lock(&(monitor->lock));
	if (monitor->stopped)
		return;
	monitor->readers--;
	if(!monitor->readers)
		pthread_cond_signal(&(monitor->OktoWrite));
	else
		pthread_cond_signal(&(monitor->OktoRead));
	pthread_mutex_unlock(&(monitor->lock));
}

void endWrite(Monitor_s* monitor){
	pthread_mutex_lock(&(monitor->lock));
	if (monitor->stopped)
		return;
	monitor->writers--;
	if(monitor->waitingReaders)
		pthread_cond_signal(&(monitor->OktoRead));
	else pthread_cond_signal(&(monitor->OktoWrite));
	pthread_mutex_unlock(&(monitor->lock));
}
