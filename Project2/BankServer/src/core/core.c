/*
 * core.c
 *
 *  Created on: Nov 28, 2014
 *      Author: user
 */

#include "core.h"
#include "util/common.h"
#include <stdlib.h>
#include <string.h>

/* id generator for new  accounts */
static int account_global;
/* accounts array */
static Account_s** accounts;
/* nr accounts */
static size_t size;
/* accounts max size */
static size_t max_size;
/* accounts access control*/
static Monitor_s* lock;

/* init accounts*/
void initAccounts(){
	size = 0;
	max_size = 10;
	accounts = (Account_s**)malloc(sizeof(Account_s*) * max_size);
	account_global = 1;
	lock = (Monitor_s*) malloc(sizeof(Monitor_s));
	initMonitor(lock);
}

/*get account with id = id */
Account_s* getAccount(int id){
	size_t i;
	startRead(lock);
	for(i = 0; i < size; i++){
		if(accounts[i]->id == id){
			endRead(lock);
			return accounts[i];
		}
	}
	endRead(lock);
	return NULL;
}

/* create a new account */
int createAccount(float balance){
	Account_s* newAcc;
	int result = OK;
	startWrite(lock);
	if(size >= max_size){
		max_size *= 2;
		accounts = realloc(accounts, max_size * sizeof(Account_s*));
		if(accounts == NULL)
			return ERROR;
	}
	newAcc = (Account_s*)malloc(sizeof(Account_s));
	if(newAcc == NULL)
		return ERROR;
	//newAcc->owner = (char*)malloc(sizeof(char)* strlen(owner) + 1);
	//strcpy(newAcc->owner, owner);
	newAcc->balance = balance;
	newAcc->id = account_global;
	newAcc->lock = (Monitor_s*) malloc(sizeof(Monitor_s));
	initMonitor(newAcc->lock);
	account_global++;
	accounts[size] = newAcc;
	size++;
	endWrite(lock);
	return result;
}

/* deposit  amount in account with id */
int deposit(int id, float amount){
	Account_s* acc;
	acc = getAccount(id);
	if(acc == NULL)
		return NO_ACCOUNT;
	startWrite(acc->lock);
	acc->balance+= amount;
	endWrite(acc->lock);
	return OK;
}

/* withdraw amount from account id */
int withdraw(int id, float amount){
	Account_s* acc;
	acc = getAccount(id);
	if(acc == NULL)
		return NO_ACCOUNT;
	startWrite(acc->lock);
	if((acc->balance - amount) < 0){
		endWrite(acc->lock);
		return NO_MONEY;
	}
	acc->balance -= amount;
	endWrite(acc->lock);
	return OK;
}

/* transfer amount from account id to account id2 */
int transfer(int id, int id2, float amount){
	int result;
	result = withdraw(id, amount);
	if(result != OK)
		return result;
	result = deposit(id2, amount);
	if(result!= OK)
		deposit(id, amount);
	return result;
}

/*get account balance */
int getAccountBalance(int id, float* balance){
	Account_s* acc;
	acc = getAccount(id);
	if(acc == NULL)
		return NO_ACCOUNT;
	startRead(acc->lock);
	*balance = acc->balance;
	endRead(lock);
	return OK;
}
