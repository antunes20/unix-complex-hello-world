/*
 * core.h
 *
 *  Created on: Nov 28, 2014
 *      Author: user
 */

#ifndef CORE_CORE_H_
#define CORE_CORE_H_

#include "util/monitor.h"

typedef struct Account{
	int id;
	float balance;
	Monitor_s* lock;
} Account_s;

/* init accounts*/
void initAccounts();

/*get account with id = id */
Account_s* getAccount(int id);

/*get account balance */
int getAccountBalance(int id, float* balance);

/* create a new account */
int createAccount(float balance);

/* deposit  amount in account with id */
int deposit(int id, float amount);

/* withdraw amount from account id */
int withdraw(int id, float amount);

/* transfer amount from account id to account id2 */
int transfer(int id, int id2, float amount);


#endif /* CORE_CORE_H_ */
