/*
 ============================================================================
 Name        : BankServer.c
 Author      : José Magalhães
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>

#include "desk/bank.h"

int main(int argc, char*argv[]) {
	int desks;
	/*if (argc != 2) {
		printf("Usage: %s <number of desks>\n", argv[0]);
		return EXIT_FAILURE;
	}
	desks = atoi(argv[1]);
	if(desks <= 0){
		printf("Number of desks must be greater than 0.\n");
		return EXIT_FAILURE;
	}*/
	desks = 10;
	init_bank(desks);
	return EXIT_SUCCESS;
}
